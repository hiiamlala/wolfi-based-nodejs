#!/bin/bash

INDEX_URL=https://nodejs.org/dist/index.json

# Function to cancel all jobs and exit
cancel_jobs() {
  echo "Cancelling all jobs..."
  for job in $(jobs -p); do
    kill "$job"
  done
  exit 1
}

# Trap Ctrl+C signal and call cancel_jobs function
trap 'cancel_jobs' SIGINT

versions=$(curl -sL "$INDEX_URL" | grep -o '"version":"[^"]*"' | grep -oE '[0-9]+\.[0-9]+\.[0-9]+' | xargs -I {} echo "https://nodejs.org/dist/v{}/node-v{}-linux-x64.tar.xz")

for version in $versions; do
  echo "Build job for verions $version"
  ./build.sh $version
done

wait
