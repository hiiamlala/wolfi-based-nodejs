#!/bin/bash

# Function to cancel all jobs and exit
cancel_jobs() {
  echo "Cancelling all jobs..."
  for job in $(jobs -p); do
    kill "$job"
  done
  exit 1
}

# Trap Ctrl+C signal and call cancel_jobs function
trap 'cancel_jobs' SIGINT

# Fetch the index.json file from the Node.js website
INDEX_URL=$(curl -sL https://nodejs.org/dist/index.json)

# Extract version numbers and filter for major versions
major_versions=$(echo "$INDEX_URL" | jq -r '.[].version' | grep -oE 'v[0-9]+' | cut -c2- | sort -u)

# Loop through major versions to find the latest for each
latest_versions=()
for major_version in $major_versions; do
  latest_version=$(echo "$INDEX_URL" | grep -oE "\"version\":\"v$major_version\.[0-9]+\.[0-9]+\"" | sort -V | tail -n 1 | grep -oE "$major_version\.[0-9]+\.[0-9]+" | xargs -I {} echo "https://nodejs.org/dist/v{}/node-v{}-linux-x64.tar.xz")
  if [ -n "$latest_version" ]; then
    latest_versions+=($latest_version)
  fi
done

for version in ${latest_versions[@]}; do
  echo "Build job for verions $version"
  ./build.sh $version $(echo $version | grep -oE '/v[0-9]+' | grep -oE '[0-9]+')
done

wait
