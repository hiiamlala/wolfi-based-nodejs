#!/bin/bash

# Check if argument is provided
if [ -z "$1" ]; then
  # Getting LTS version
  NODE_PACKAGE_URL=$(curl -sL https://nodejs.org/dist/index.json | grep -o '"version":"[^"]*"' | grep -oE '[0-9]+\.[0-9]+\.[0-9]+' | sort -V | tail -n 1 | xargs -I {} echo "https://nodejs.org/dist/v{}/node-v{}-linux-x64.tar.xz")
  echo "LTS version URL: $NODE_PACKAGE_URL"
else
  # Use the provided URL
  NODE_PACKAGE_URL="$1"
  echo "Provided URL: $NODE_PACKAGE_URL"
fi

TEMP_DIR=./tmp
version=$(echo $NODE_PACKAGE_URL | grep -oE 'v([0-9]+\.){2}[0-9]+' | head -n1)
filename=$(basename "$NODE_PACKAGE_URL" | awk -F '?' '{print $1}' | awk -F '#' '{print $1}')
dependencies=("tar" "wget", "docker")

if [ -z "$2" ]; then
  image_tag=$verion
else
  image_tag=$2
fi

echo -n "Checking dependencies... "
for name in $dependencies; do
  [[ $(which $name 2>/dev/null) ]] || {
    echo -en "\n$name needs to be installed."
    deps=1
  }
done
[[ $deps -ne 1 ]] && echo "OK" || {
  echo -en "\nInstall the above and rerun this script\n"
  exit 1
}

wget -q -O $TEMP_DIR/$filename $NODE_PACKAGE_URL

mkdir -p $TEMP_DIR/$version

tar -xf $TEMP_DIR/$filename -C $TEMP_DIR/$version

extracted_location=$(ls -d $TEMP_DIR/$version/*/)
extracted_dir=$(basename $extracted_location)

if [[ $(docker build --build-arg NODE_DIR=$extracted_dir --build-arg NODE_LOCATION=$extracted_location -t dso-base/node:$image_tag .) ]]; then
  echo """Image successfully built "dso-base/node:$image_tag""""
else
  echo "Build failed!"
  exit 1
fi

rm -rf $TEMP_DIR/$version
